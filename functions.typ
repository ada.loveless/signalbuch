// Signale sind kompliziert.
//  Im wesentlichen sind sie nur definiert als "Ein Signal ist ein sichtbares oder hörbares Zeichen mit einer festgelegten Information zur Gewährleistung des sicheren Bewegens von Eisenbahnfahrzeugen."
//  Es kann also mehrere Objekte geben die alle das gleiche Signal sind.
//  Z.b. Hp 0 hat eine Tag- und Nachtform und Form# sowie Lichtsignale.
//

#let signal(short, description, meaning) = [
  === #short 
  / Bedeutung: #meaning
  #description
]
