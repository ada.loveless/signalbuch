#import("functions.typ"): signal

= Signale nach Signalbegriffen

#signal(
  "So 20", 
  "Ein schwarzes Rechteck mit weißem Dreieck", 
  "Das durch die Zuordnungstafel gekennzeichnete Signal gilt für das Gleis, auf das die Spitze des Dreiecks weist."
)

== Hp (Hauptsignale)

#signal(
  "Hp 0", 
  text[
    / Formsignal, Tageszeichen: Ein Signalflügel – bei zweiflügligen Signalen der obere Flügel – zeigt waagerecht nach rechts.
    / Formsignal, Nachtzeichen: Ein rotes Licht
    / Lichtsignal: Ein rotes Licht oder zwei rote Lichter nebeneinander.
    ],
  "Halt"
)

#signal(
  "Hp 1", 
  text[
    / Formsignal, Tageszeichen: Ein Signalflügel – bei zweiflügligen Signalen der obere Flügel – zeigt schräg nach rechts aufwärts.
    / Formsignal, Nachtzeichen: Ein grünes Licht.
    / Lichtsignal: Ein grünes Licht.
    ],
  "Fahrt"
)

#signal(
  "Hp 2",
  text[
    / Formsignal, Tageszeichen: Zwei Signalflügel zeigen schräg nach rechts aufwärts.
    / Formsignal, Nachtzeichen: Ein grünes und senkrecht darunter ein gelbes Licht.
    / Lichtsignal: Ein grünes und senkrecht darunter ein gelbes Licht.
  ],
  "Langsamfahrt"
)

== Ks (Kombinationssignale)

#signal(
  "Ks 1",
  text[
    / Lichtsignal: Ein grünes Licht bzw. ein grünes Blinklicht.
  ],
  "Fahrt"
)

#signal(
  "Ks 2",
  text[
    / Lichtsignal: Ein gelbes Licht.
  ],
  "Halt erwarten"
)

== Hl (Lichthaupt und Lichtvorsignale)

- Nur DV301 -> Nochmal abchecken?

#signal(
  "Hl 1",
  text[
    / Lichtsignal: Ein grünes Licht.
  ],
  "Fahrt mit Höchstgeschwindigkeit."
)

#signal(
  "Hl 2",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem grünen Lichtstreifen, darüber ein grünes Licht.
  ],
  "Fahrt mit 100 km/h, dann mit Höchstgeschwindigkeit."
)

#signal(
  "Hl 3a",
  text[
    / Lichtsignal: Ein gelbes Licht, darüber ein grünes Licht.
  ],
  "Fahrt mit 40 km/h, dann mit Höchstgeschwindigkeit."
)

#signal(
  "Hl 3b",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem gelben Lichtstreifen, darüber ein grünes Licht.
  ],
  "Fahrt mit 60 km/h, dann mit Höchstgeschwindigkeit."
)

#signal(
  "Hl 4",
  text[
    / Lichtsignal: Ein grünes Blinklicht.
  ],
  "Höchstgeschwindigkeit auf 100 km/h ermäßigen."
)

#signal(
  "Hl 5",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem grünen Lichtstreifen, darüber ein grünes Blinklicht.
  ],
  "Fahrt mit 100 km/h."
)

#signal(
  "Hl 6a",
  text[
    / Lichtsignal: Ein gelbes Licht, darüber ein grünes Blinklicht.
  ],
  "Fahrt mit 40 km/h, dann mit 100 km/h."
)

#signal(
  "Hl 6b",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem gelben Lichtstreifen, darüber ein grünes Blinklicht.
  ],
  "Fahrt mit 60 km/h, dann mit 100 km/h."
)

#signal(
  "Hl 7",
  text[
    / Lichtsignal: Ein gelbes Blinklicht.
  ],
  "Höchstgeschwindigkeit auf 40 km/h (60 km/h) ermäßigen."
)

#signal(
  "Hl 8",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem grünen Lichtstreifen, darüber ein gelbes Blinklicht.
  ],
  "Geschwindigkeit 100 km/h auf 40 km/h (60 km/h) ermäßigen."
)

#signal(
  "Hl 9a",
  text[
    / Lichtsignal: Ein gelbes Licht, darüber ein gelbes Blinklicht.
  ],
  "Fahrt mit 40 km/h, dann mit 40 km/h (60 km/h)."
)

#signal(
  "Hl 9b",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem gelben Lichtstreifen, darüber ein gelbes Blinklicht.
  ],
  "Fahrt mit 60 km/h, dann mit 40 km/h (60 km/h)."
)

#signal(
  "Hl 10",
  text[
    / Lichtsignal: Ein gelbes Licht.
  ],
  "Halt erwarten."
)

#signal(
  "Hl 11",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem grünen Lichtstreifen, darüber ein gelbes Licht.
  ],
  "Geschwindigkeit 100 km/h ermäßigen, „Halt“ erwarten."
)

#signal(
  "Hl 12a",
  text[
    / Lichtsignal: Zwei gelbe Lichter übereinander.
  ],
  "Geschwindigkeit 40 km/h ermäßigen, „Halt“ erwarten."
)

#signal(
  "Hl 12b",
  text[
    / Lichtsignal: Ein gelbes Licht mit einem gelben Lichtstreifen, darüber ein gelbes Licht.
  ],
  "Geschwindigkeit 60 km/h ermäßigen, „Halt“ erwarten."
)

== Sv (Haupt und Vorsignalverbindungen)

#signal(
  "Sv 0",
  text[
    / Lichtsignal: Zwei gelbe Lichter waagerecht nebeneinander.
  ],
  "Zughalt! Weiterfahrt auf Sicht.",
)

#signal(
  "Sv 1",
  text[
    / Lichtsignal: Zwei grüne Lichter waagerecht nebeneinander.
  ],
  "Fahrt! Fahrt erwarten."
)

#signal(
  "Sv 2",
  text[
    / Lichtsignal: Ein grünes, rechts daneben in gleicher Höhe ein gelbes Licht.
  ],
  "Fahrt! Halt erwarten."
)

#signal(
  "Sv 3",
  text[
    / Lichtsignal: Links ein grünes Licht; rechts in gleicher Höhe ein grünes und senkrecht darunter ein gelbes Licht.
  ],
  "Fahrt! Langsamfahrt erwarten."
)

#signal(
  "Sv 4",
  text[
    / Lichtsignal: Links ein grünes und senkrecht darunter ein gelbes Licht; rechts in Höhe des oberen linken Lichtes ein grünes Licht.
  ],
  "Langsamfahrt! Fahrt erwarten."
)

#signal(
  "Sv 5",
  text[
    / Lichtsignal: Links ein grünes und senkrecht darunter ein gelbes Licht; rechts daneben in gleicher Höhe die gleichen Lichter.
  ],
  "Langsamfahrt! Langsamfahrt erwarten."
)

#signal(
  "Sv 6",
  text[
    / Lichtsignal: Links ein grünes, senkrecht darunter ein gelbes Licht; rechts in Höhe des oberen linken Lichtes ein gelbes Licht.
  ],
  "Langsamfahrt! Halt erwarten."
)

== Vr (Vorsignale)

#signal(
  "Vr 0",
  text[
    / Formsignal, Tageszeichen: Die runde Scheibe steht senkrecht. Wo ein Flügel vorhanden ist, zeigt er senkrecht nach unten. 
    / Formsignal, Nachtzeichen: Zwei gelbe Lichter nach rechts steigend.
    / Lichtsignal: Zwei gelbe Lichter nach rechts steigend.
    // Weird in DV301: Ein licht ist manchmal auch okay. How to deal with these?
  ],
  "Halt erwarten."
)

#signal(
  "Vr 1",
  text[
    / Formsignal, Tageszeichen: Die runde Scheibe liegt waagerecht.
    / Formsignal, Nachtzeichen: Zwei grüne Lichter nach rechts steigend.
    / Lichtsignal: Zwei grüne Lichter nach rechts steigend.
    // Weird in DV301: Ein licht ist auch okay, manchmal. How to deal with these?
  ],
  "Fahrt erwarten."
)

#signal(
  "Vr 2",
  text[
    / Formsignal, Tageszeichen: Die runde Scheibe steht senkrecht, der Flügel zeigt schräg nach rechts abwärts.
    / Formsignal, Nachtzeichen: Ein gelbes Licht und nach rechts steigend ein grünes Licht.
    / Lichtsignal: Ein gelbes Licht und nach rechts steigend ein grünes Licht.
    // DV301 darf die reihenfolge umdrehen
  ],
  "Langsamfahrt erwarten."
)

#signal(
  "Vr 1/2",
  text[
    / Formsignal, Tageszeichen: Die runde Scheibe liegt waagerecht.
    / Formsignal, Nachtzeichen: Zwei grüne Lichter nach rechts steigend.
    // DV301 darf die reihenfolge umdrehen
  ],
  "Fahrt oder Langsamfahrt erwarten."
)

- Das Signal Vr 1/2 wird nur an zweibegriffigen Formvorsignalen gezeigt.

== Zs (Zusatzsignale)

#signal(
  "Zs 1",
  text[
    / Beschreibung: Drei weiße Lichter in Form eines A oder ein weißes Blinklicht.
  ],
  "Am Signal Hp 0 oder am gestörten Lichthauptsignal ohne schriftlichen Befehl vorbeifahren."
)

#signal(
  "Zs 2",
  text[
    / Beschreibung: Ein weißleuchtender Buchstabe. Der Infrastrukturunternehmer gibt die verwendeten Kennbuchstaben bekannt.
  ],
  "Die Fahrstraße führt in die angezeigte Richtung."
)

#signal(
  "Zs 2v",
  text[
    / Beschreibung: Ein gelbleuchtender Buchstabe. Der Infrastrukturunternehmer gibt die verwendeten Kennbuchstaben bekannt.
  ],
  "Richtungsanzeiger (Zs 2) erwarten."
)

#signal(
  "Zs 3",
  text[
    / Formsignal: Eine weiße Kennziffer auf dreieckiger schwarzer Tafel mit weißem Rand. Die Tafel steht in der Regel auf der Spitze; bei beschränktem Raum kann die Spitze nach oben zeigen.
  ],
  "Die durch die Kennziffer angezeigte Geschwindigkeit darf vom Signal ab im anschließenden Weichenbereich nicht überschritten werden."
)

#signal(
  "Zs 3",
  text[
    / Formsignal: Eine weiße Kennziffer auf dreieckiger schwarzer Tafel mit weißem Rand. Die Tafel steht in der Regel auf der Spitze; bei beschränktem Raum kann die Spitze nach oben zeigen.
    / Lichtsignal: Eine weiß leuchtende Kennziffer.
  ],
  "Die durch die Kennziffer angezeigte Geschwindigkeit darf vom Signal ab im anschließenden Weichenbereich nicht überschritten werden."
)

#signal(
  "Zs 3v",
  text[
    / Formsignal: Eine gelbe Kennziffer auf dreieckiger schwarzer Tafel mit gelbem Rand.
    / Lichtsignal: Eine gelbleuchtende Kennziffer.
  ],
  "Geschwindigkeitsanzeiger (Zs 3) erwarten"
)

#signal(
  "Zs 6",
  text[
    / Formsignal: Eine rechteckige schwarze Scheibe mit weißem Rand und einem weißen von rechts nach links steigenden Streifen, dessen Enden senkrecht abgewinkelt sind. Das Formsignal ist rückstrahlend.
    / Lichtsignal: Ein weiß leuchtender schräger Lichtstreifen, dessen Enden in der Regel senkrecht nach oben und unten abgebogen sind.
  ],
  "Der Fahrweg führt in das Streckengleis entgegen der gewöhnlichen Bedeutung Fahrtrichtung."
)

#signal(
  "Zs 7",
  text[
    / Beschreibung: Drei gelbe Lichter in Form eines V.
  ],
  "Am Signal Hp 0 oder am gestörten Lichthauptsignal ohne schriftlichen Befehl vorbeifahren! Weiterfahrt auf Sicht."
)

#signal(
  "Zs 8",
  text[
    / Beschreibung: Drei blinkende weiße Lichter in Form eines A oder ein weißblinkender Lichtstreifen von rechts nach links steigend.
  ],
  "Am Halt zeigenden oder gestörten Hauptsignal vorbeifahren, der Fahrweg führt in das Streckengleis entgegen der gewöhnlichen Fahrtrichtung."
)

#signal(
  "Zs 9",
  text[
    / Beschreibung: Eine dreieckige, weiße Tafel mit rotem Rand und schwarzem Gatter.
  ],
  "Nach dem zulässigen Vorbeifahren an dem Halt zeigenden oder gestörten Lichthauptsignal Halt vor dem Bahnübergang! Weiterfahrt nach Sicherung."
)

#signal(
  "Zs 10",
  text[
    / Formsignal: Ein weißer Pfeil mit der Spitze nach oben auf pfeilförmiger, schwarzer Tafel.
    / Lichtsignal: Ein weißleuchtender Pfeil mit der Spitze nach oben.
  ],
  "Ende der Geschwindigkeitsbeschränkung."
)

#signal(
  "Zs 12",
  text[
    / Beschreibung: Eine weiße Tafel mit rotem Rand und rotem „M“ in Schreibschrift.
  ],
  "Am Halt zeigenden oder gestörten Hauptsignal auf mündlichen oder fernmündlichen Auftrag vorbeifahren."
)

#signal(
  "Zs 13",
  text[
    / Formsignal: Ein um 90° nach links umgelegtes gelbes rückstrahlendes „T“ auf einer rechteckigen schwarzen Tafel.
    / Lichtsignal: Ein um 90° nach links umgelegtes gelbleuchtendes „T“.
  ],
  "Fahrt in ein Stumpfgleis oder in ein Gleis mit verkürztem Einfahrweg."
)

#signal(
  "Zs 103 (DV 301)",
  text[
    / Beschreibung: Eine rechteckige schwarze Tafel mit weißen Rauten. Die Rautentafel ist am Hauptsignal angebracht.
  ],
  "Das Halt zeigende Hauptsignal gilt nicht für Rangierabteilungen."
)

== Ts (Signale für Schiebelokomotiven und Sperrfahrten)

#signal(
  "Ts 1",
  text[
    / Beschreibung: Um 90° nach rechts umgelegtes weißes T auf schwarzer Rechteckscheibe.
  ],
  "Nachschieben einstellen."
)

#signal(
  "Ts 2",
  text[
    / Beschreibung: Quadratische, auf der Spitze stehende weiße Scheibe mit schwarzem Rand.
  ],
  "Halt für zurückkehrende Schiebelokomotiven und Sperrfahrten.."
)

#signal(
  "Ts 3",
  text[
    / Beschreibung: Auf Signal Ts 2 ein schwarzer nach rechts steigender Streifen.
  ],
  "Weiterfahrt für zurückkehrende Schiebelokomotiven und Sperrfahrten."
)

== Lf (Langsamfahrsignale)

#signal(
  "Lf 1",
  text[
    / Tageszeichen: Eine auf der Spitze stehende dreieckige gelbe Scheibe mit weißem Rand zeigt eine schwarze Kennziffer. Bei beschränktem Raum kann die Dreieckspitze nach oben zeigen.
    / Nachtzeichen: Unter dem beleuchteten Tageszeichen zwei schräg nach links steigende gelbe Lichter. Bei beschränktem Raum befinden sich die Lichter vor dem Tageszeichen.
  ],
  "Es folgt eine vorübergehende Langsamfahrstelle, auf der die angezeigte Geschwindigkeit nicht überschritten werden darf."
)

#signal(
  "Lf 1/2",
  text[
    / Beschreibung: Eine rechteckige, gelbe Scheibe mit weißem Rand zeigt eine schwarze Kenn-
ziffer.
  ],
  "Auf dem am Signal beginnenden, in der Regel durch eine Endscheibe begrenzten Gleisabschnitt darf die angezeigte Geschwindigkeit nicht überschritten werden."
)

#signal(
  "Lf 2",
  text[
    / Beschreibung: Eine rechteckige, auf der Schmalseite stehende oder quadratische gelbe Scheibe mit weißem Rand und schwarzem A.
  ],
  "Anfang der vorübergehenden Langsamfahrstelle."
)

#signal(
  "Lf 3",
  text[
    / Beschreibung: Eine rechteckige, auf der Schmalseite stehende oder quadratische weiße Scheibe mit schwarzem E.
  ],
  "Ende der vorübergehenden Langsamfahrstelle."
)

#signal(
  "Lf 4",
  text[
    / Beschreibung: Eine auf der Spitze stehende dreieckige weiße Tafel mit schwarzem Rand zeigt eine schwarze Kennziffer. Bei beschränktem Raum kann die Dreieckspitze nach oben zeigen
  ],
  "Es folgt eine ständige Langsamfahrstelle, auf der die angezeigte Geschwindigkeit nicht überschritten werden darf."
)

#signal(
  "Lf 5",
  text[
    / Beschreibung: Eine rechteckige, auf der Schmalseite stehende weiße Tafel mit schwarzem A.
  ],
  "Die auf der Geschwindigkeitstafel (Lf 4) angezeigte Geschwindigkeitsbeschränkung muss durchgeführt sein."
)

#signal(
  "Lf 6",
  text[
    / Beschreibung: Eine auf der Spitze stehende, schwarz- und weißumrandete dreieckige gelbe Tafel zeigt eine schwarze Kennziffer.
  ],
  "Ein Geschwindigkeitssignal (Lf 7) ist zu erwarten."
)

#signal(
  "Lf 7",
  text[
    / Beschreibung: Eine rechteckige, auf der Schmalseite stehende oder quadratische weiße Tafel mit schwarzem Rand zeigt eine schwarze Kennziffer.
  ],
  "Die angezeigte Geschwindigkeit darf vom Signal ab nicht überschritten werden."
)

== Sh (Schutzsignale)

#signal(
  "Sh 0",
  text[
    / Beschreibung: Ein waagerechter schwarzer Streifen in runder weißer Scheibe auf schwarzem Grund.
  ],
  "Halt! Fahrverbot."
)

#signal(
  "Sh 1",
  text[
    / Formsignal: Ein nach rechts steigender schwarzer Streifen auf runder weißer Scheibe.
    / Lichtsignal: Zwei weiße Lichter nach rechts steigend.
  ],
  "Fahrverbot aufgehoben."
)

#signal(
  "Sh 2",
  text[
    / Tageszeichen: Eine rechteckige rote Scheibe mit weißem Rand.
    / Nachtzeichen: Ein rotes Licht am Tageszeichen oder am Ausleger des Wasserkrans.
  ],
  "Schutzhalt."
)

#signal(
  "Sh 3",
  text[
    / Tageszeichen: Eine rot-weiße Signalfahne, irgendein Gegenstand oder der Arm wird im Kreis geschwungen.
    / Nachtzeichen: Eine Laterne, möglichst rot abgeblendet, oder ein leuchtender Gegenstand wird im Kreis geschwungen. 
  ],
  "Sofort halten."
)

#signal(
  "Sh 5",
  text[
    / Beschreibung: Mehrmals nacheinander drei kurze Töne.
  ],
  "Sofort halten."
)

== Ra (Rangiersignale)

#signal(
  "Ra 1",
  text[
    / Beschreibung: Ein langer Ton.
    / Tagzeichen: Senkrechte Bewegung des Arms von oben nach unten.
    / Nachtzeichen: Senkrechte Bewegung der Laterne von oben nach unten. 
  ],
  "Wegfahren."
)

#signal(
  "Ra 2",
  text[
    / Beschreibung: Zwei mäßig lange Töne.
    / Tagzeichen: Langsame waagerechte Bewegung des Arms hin und her. 
    / Nachtzeichen: Langsame waagerechte Bewegung der Laterne hin und her.
  ],
  "Herkommen."
)

#signal(
  "Ra 3",
  text[
    / Beschreibung: Zwei kurze Töne schnell nacheinander.
    / Tagzeichen: Beide Arme in Schulterhöhe nach vorn heben und die flach ausgestreckten Hände wiederholt einander nähern.
    / Nachtzeichen: Wie am Tage, in der einen Hand eine Laterne.
  ],
  "Aufdrücken."
)

#signal(
  "Ra 4",
  text[
    / Beschreibung: Zwei lange Töne und ein kurzer Ton.
    / Tagzeichen: Zweimal eine waagerechte Bewegung des Arms vom Körper nach außen und eine schnelle senkrechte Bewegung nach unten. 
    / Nachtzeichen: Zweimal eine waagerechte Bewegung der Laterne vom Körper nach außen und eine schnelle senkrechte Bewegung nach unten. 
  ],
  "Abstoßen."
)

#signal(
  "Ra 5",
  text[
    / Beschreibung: Drei kurze Töne schnell nacheinander.
    / Tagzeichen: Kreisförmige Bewegung des Arms.
    / Nachtzeichen: Kreisförmige Bewegung der Handlaterne.
  ],
  "Rangierhalt."
)

#signal(
  "Ra 6",
  text[
    / Formsignal: Ein waagerechter weißer Balken mit schwarzem Rand.
    / Lichtsignal: Ein waagerechter weißer Lichtstreifen. 
  ],
  "Halt! Abdrücken verboten."
)

#signal(
  "Ra 7",
  text[
    / Formsignal: Ein weißer Balken mit schwarzem Rand schräg nach rechts aufwärts.
    / Lichtsignal: Ein weißer Lichtstreifen schräg nach rechts aufwärts.
  ],
  "Langsam abdrücken."
)

#signal(
  "Ra 8",
  text[
    / Formsignal: Ein senkrechter weißer Balken mit schwarzem Rand.
    / Lichtsignal: Ein senkrechter Lichtstreifen. 
  ],
  "Mäßig schnell abdrücken."
)

#signal(
  "Ra 9",
  text[
    / Lichtsignal: Ein senkrechter Lichtstreifen, vom oberen Ende nach rechts abzweigend ein waagerechter Lichtstreifen
  ],
  "Zurückziehen."
)

#signal(
  "Ra 10",
  text[
    / Beschreibung: Eine oben halbkreisförmig abgerundete weiße Tafel mit schwarzer Aufschrift ”Halt für Rangierfahrten”. Die weiße Tafel kann auch ohne Aufschrift sein.
  ],
  "Über die Tafel hinaus darf nicht rangiert werden."
)

#signal(
  "Ra 11",
  text[
    / Beschreibung Ra11, Ra11a (DS 301): Ein gelbes W mit schwarzem Rand.
    / Beschreibung Ra11b (DV 301): Ein weißes W mit schwarzem Rand.
  ],
  "Auftrag des Wärters zur Rangierfahrt abwarten."
)

#signal(
  "Ra 12",
  text[
    / Beschreibung: Ein rot-weißes Zeichen.
  ],
  "Grenze, bis zu der bei zusammenlaufenden Gleisen das Gleis besetzt werden darf."
)

#signal(
  "Ra 13",
  text[
    / Beschreibung: Auf weißem Grund ein blauer Pfeil.
  ],
  "Kennzeichnung der Grenze der Gleisisolierung."
)

== Wn (Weichensignale)

#signal(
  "Wn 1",
  text[
    / Formsignal: Ein auf der Schmalseite stehendes weißes Rechteck auf schwarzem Grund.
    / Lichtsignal: Zwei übereinander stehende weiße Lichter.
  ],
  "Gerader Zweig."
)

#signal(
  "Wn 2",
  text[
    / Formsignal: Ein weißer Pfeil oder Streifen auf schwarzem Grund zeigt entsprechend der Ablenkung schräg nach links oder rechts aufwärts.
    / Lichtsignal: Zwei nebeneinander stehende weiße Lichter.
    / Vom Herzstück gesehen, bei einfachen Weichen und Innenbogenweichen: Eine runde weiße Scheibe auf schwarzem Grund.
    / Vom Herzstück gesehen, bei Außenbogenweichen: Eine nach links oder rechts geöffnete Sichel auf runder weißer Scheibe mit schwarzem Grund.
  ],
  "Gebogener Zweig."
)

#signal(
  "Wn 3",
  text[
    / Formsignal: Die Pfeile oder Streifen bilden eine von links nach rechts steigende Linie.
    / Lichtsignal: Die Lichter bilden eine von links nach rechts steigende Linie. 
  ],
  "Gerade von links nach rechts."
)

#signal(
  "Wn 4",
  text[
    / Formsignal: Die Pfeile oder Streifen bilden eine von rechts nach links steigende Linie.
    / Lichtsignal: Die Lichter bilden eine von rechts nach links steigende Linie.
  ],
  "Gerade von rechts nach links."
)

#signal(
  "Wn 5",
  text[
    / Formsignal: Die Pfeile oder Streifen bilden einen nach links geöffneten rechten Winkel.
    / Lichtsignal: Die Lichter bilden einen nach links geöffneten rechten Winkel.
  ],
  "Bogen von links nach links."
)

#signal(
  "Wn 6",
  text[
    / Formsignal: Die Pfeile oder Streifen bilden einen nach rechts geöffneten rechten Winkel.
    / Lichtsignal: Die Lichter bilden einen nach rechts geöffneten rechten Winkel.
  ],
  "Bogen von rechts nach rechts."
)

#signal(
  "Wn 7",
  text[
    / Beschreibung: Ein senkrechter, schwarzer Streifen in einer runden, weißen Scheibe auf schwarzem Grund
  ],
  "Die Gleissperre ist abgelegt."
)

== Zp (Signale für das Zugpersonal)

#signal(
  "Zp 1",
  text[
    / Beschreibung: Ein mäßig langer Ton.
  ],
  "Achtung."
)

#signal(
  "Zp 2",
  text[
    / Beschreibung: Ein kurzer Ton.
  ],
  "Handbremsen mäßig anziehen."
)

#signal(
  "Zp 3",
  text[
    / Beschreibung: Drei kurze Töne schnell hintereinander.
  ],
  "Handbremse stark anziehen."
)

#signal(
  "Zp 4",
  text[
    / Beschreibung: Zwei mäßig lange Töne nacheinander.
  ],
  "Handbremsen lösen."
)

#signal(
  "Zp 5",
  text[
    / Beschreibung: Mehrmals drei kurze Töne schnell nacheinander.
  ],
  "Beim Zug ist etwas Außergewöhnliches eingetreten - Bremsen und Hilfe leisten."
)

#signal(
  "Zp 6",
  text[
    / Handsignal, Tageszeichen: Beide Hände werden über dem Kopf zusammengeschlagen. 
    / Handsignal, Nachtzeichen: Die weißleuchtende Handlaterne wird mehrmals mit der rechten Hand in einem Halbkreis gehoben und senkrecht schnell gesenkt.
    / Lichtsignal: Ein weißes Licht.
  ],
  "Bremse anlegen.",
)

#signal(
  "Zp 7",
  text[
    / Handsignal, Tageszeichen: Eine Hand wird über dem Kopf mehrmals im Halbkreis hin- und hergeschwungen. 
    / Handsignal, Nachtzeichen: Die weißleuchtende Handlaterne wird über dem Kopf mehrmals im Halbkreis hin- und hergeschwungen. 
    / Lichtsignal: Zwei weiße Lichter senkrecht übereinander. 
  ],
  "Bremse lösen.",
)

#signal(
  "Zp 8",
  text[
    / Handsignal, Tageszeichen: Beide Arme werden gestreckt senkrecht hochgehalten.
    / Handsignal, Nachtzeichen: Die weißleuchtende Handlaterne wird mehrmals in Form einer liegenden Acht bewegt. 
    / Lichtsignal: Drei weiße Lichter senkrecht übereinander. 
  ],
  "Bremse in Ordnung.",
)

#signal(
  "Zp 9",
  text[
    / Handsignal, Tageszeichen: Eine runde weiße Scheibe mit grünem Rand.
    / Handsignal, Nachtzeichen: Ein grünes Licht.
    / Lichtsignal: Ein grünleuchtender Ring.
    / Auch: Ein senkrechter grüner Lichtstreifen. Dieses Signal wird neu nicht mehr aufgestellt.
  ],
  "Abfahren.",
)

#signal(
  "Zp 10",
  text[
    / Beschreibung: Ein waagerechter, weißer Lichtstreifen.
  ],
  "Türen schließen.",
)

#signal(
  "Zp 11",
  text[
    / Beschreibung: Ein langer, ein kurzer und ein langer Ton oder ein langes, ein kurzes und ein langes Lichtzeichen.
  ],
  "Kommen.",
)

#signal(
  "Zp 12",
  text[
    / Beschreibung: Zwei kurze, ein langer und ein kurzer Ton.
  ],
  "Grenzzeichenfrei.",
)

== El (Fahrleitungssignale)

/ Beschreibung: Die Fahrleitungssignale bestehen aus einer auf der Spitze stehenden, weiß und schwarz umrandeten blauen quadratischen Tafel mit weißen Signalzeichen.

=== El 1 v

#signal(
  "El 1 v",
  text[
    / Beschreibung: Zwei weiße Rechtecke waagerecht nebeneinander.
  ],
  "Signal El 1 erwarten.",
)

#signal(
  "El 1",
  text[
    / Beschreibung: Ein zerlegtes weißes U.
  ],
  "Ausschalten.",
)

#signal(
  "El 2",
  text[
    / Beschreibung: Ein geschlossenes weißes U.
  ],
  "Einschalten erlaubt.",
)

#signal(
  "El 3",
  text[
    / Beschreibung: Zwei in der Höhe gegeneinander versetzte weiße Streifen.
  ],
  "Signal „Bügel ab“ erwarten.",
)

#signal(
  "El 4",
  text[
    / Beschreibung: Ein waagerechter weißer Streifen.
  ],
  "Bügel ab."
)

#signal(
  "El 5",
  text[
    / Beschreibung: Ein senkrechter weißer Streifen.
  ],
  "Bügel an."
)

#signal(
  "El 6",
  text[
    / Beschreibung: Ein auf der Spitze stehender quadratischer weißer Rahmen mit innenliegendem weißem Quadrat.
  ],
  "Halt für Fahrzeuge mit gehobenen Stromabnehmern."
)

== Zg (Signale an Zügen)

#signal(
  "Zg 1",
  text[
    / Tageszeichen: Kein besonderes Signal.
    / Nachtzeichen: Vorn am ersten Fahrzeug, wenn dieses ein Triebfahrzeug oder Steuerwa-gen ist, drei weiße Lichter in Form eines A (Dreilicht-Spitzensignal).
    / Nachtzeichen: Vorn am ersten Fahrzeug, wenn dieses nicht ein Triebfahrzeug oder Steuerwagen ist, zwei weiße Lichter in gleicher Höhe.
  ],
  "Kennzeichnung der Zugspitze.",
)

#signal(
  "Zg 2",
  text[
    / Beschreibung: Am letzten Fahrzeug zwei rote Lichter, oder zwei rechteckige reflektierende Schilder mit weißen Dreiecken seitlich und je einem roten Dreieck oben und unten, die mit ihren Spitzen sich in der Mitte des Schildes berühren. Die roten Lichter dürfen blinken.
  ],
  "Kennzeichnung des Zugschlusses.",
)

== Fz (Signale an einzigen Fahrzeugen)

#signal(
  "Fz 1",
  text[
    / Tageszeichen: Kein besonderes Signal.
    / Nachtzeichen: Vorn und hinten ein weißes Licht, in der Regel in Höhe der Puffer. Stattdessen kann auch das Signal Zg 1a geführt werden; es muss geführt werden, wenn Bahnübergänge ohne technische Sicherung oder ohne Sicherung durch Posten befahren werden.
  ],
  "Kennzeichnung einer Lokomotive im Rangierdienst.",
)

#signal(
  "Fz 2",
  text[
    / Tageszeichen: An jeder Langseite des Wagens eine gelbe Fahne oder gelbe Tafel.
    / Nachtzeichen: Das Tageszeichen; außerdem der Wagen nach außen erkennbar im Inneren beleuchtet.
  ],
  "Kennzeichnung von Wagen, die während eines Stilllagers mit Personal besetzt sind.",
)

== Ro (Rottenwarnsignale)

#signal(
  "Ro 1",
  text[
    / Beschreibung: Mit dem Horn ein langer Ton als Mischklang aus zwei verschieden hohen Tönen.
  ],
  "Vorsicht! Im Nachbargleis nähern sich Fahrzeuge."
)

#signal(
  "Ro 2",
  text[
    / Beschreibung: Mit dem Horn zwei lange Töne nacheinander in verschiedener Tonlage.
  ],
  "Arbeitsgleise räumen."
)

#signal(
  "Ro 3",
  text[
    / Beschreibung: Mit dem Horn mindestens fünfmal je zwei kurze Töne nacheinander in verschiedener Tonlage.
  ],
  "Arbeitsgleise schnellstens räumen.",
)

#signal(
  "Ro 4",
  text[
    / Beschreibung: Ein weißes Fahnenschild mit schwarzem Rand.
  ],
  "Kennzeichnung der Gleisseite, nach der beim Ertönen der Rottenwarnsignale Ro 2 und Ro 3 die Arbeitsgleise zu räumen sind.",
)

== Ne (Nebensignale)

#signal(
  "Ne 1",
  text[
    / Beschreibung: Eine weiße Trapeztafel mit schwarzem Rand an schwarz und weiß schräg gestreiftem Pfahl.
  ],
  "Kennzeichnung der Stelle, wo bestimmte Züge vor einer Betriebsstelle zu halten haben.",
)

#signal(
  "Ne 2",
  text[
    / Beschreibung: Eine schwarzgeränderte weiße Tafel mit zwei übereinander stehenden schwarzen Winkeln, die sich mit der Spitze berühren.
    / Dreibegriffiges Vorsignal: Als Kennzeichnung des Standorts eines dreibegriffigen Formvorsignals im Geltungsbereich der DV 301 kann über der Vorsignaltafel eine dreieckige, schwarzgeränderte, weiße Tafel mit einem schwarzen Punkt angebracht sein.
  ],
  "Kennzeichnung des Standorts eines Vorsignals.",
)

Sehr kompliziert, mehr dazu irgendwann?

#signal(
  "Ne 3",
  text[
    / Beschreibung: Mehrere aufeinanderfolgende viereckige weiße Tafeln mit einem oder mehre-ren nach rechts steigenden schwarzen Streifen, deren Anzahl in der Fahrtrichtung abnimmt. Bei Dunkelheit und unsichtigem Wetter können zusätzlich rückstrahlende weiße Streifen erscheinen, deren Anzahl und Anordnung den schwarzen Streifen entspricht.
  ],
  "Ein Vorsignal ist zu erwarten.",
)

#signal(
  "Ne 4",
  text[
    / Beschreibung: Eine viereckige, schachbrettartig schwarz und weiß gemusterte Tafel.
  ],
  "Das Hauptsignal steht – abweichend von der Regel – an einem anderen Standort."
)

#signal(
  "Ne 5",
  text[
    / Beschreibung: Eine hochstehende weiße Rechteckscheibe mit schwarzem Rand und schwarzem H oder eine hochstehende schwarze Rechteckscheibe mit weißem H.
H
  ],
  "Kennzeichnung des Halteplatzes der Zugspitze bei planmäßig haltenden Zügen."
)

#signal(
  "Ne 6",
  text[
    / Beschreibung: Eine schräg zum Gleis gestellte waagerechte weiße Tafel mit drei schwarzen Schrägstreifen.
H
  ],
  "Ein Haltepunkt ist zu erwarten.",
)

#signal(
  "Ne 7a",
  text[
    / Beschreibung: Eine weiße Pfeilspitze mit schwarzem Rand zeigt nach oben oder eine gelbe Pfeilspitze mit schwarzem Rand zeigt nach oben.
  ],
  "Pflugschar heben.",
)

#signal(
  "Ne 7b",
  text[
    / Beschreibung: Eine weiße Pfeilspitze mit schwarzem Rand zeigt nach unten oder eine gelbe Pfeilspitze mit schwarzem Rand zeigt nach unten.
  ],
  "Pflugschar senken.",
)

#signal(
  "Ne 12",
  text[
    / Beschreibung: Eine rechteckige, orangefarbene Tafel mit zwei waagerechten weißen Streifen.
  ],
  "Überwachungssignal einer Rückfallweiche beachten.",
)

#signal(
  "Ne 13a",
  text[
    / Beschreibung: Ein weißes Licht über einem orangefarbenen waagerechten Streifen und einem orange-weiß schräg gestreiften Mastschild.
  ],
  "Die Rückfallweiche ist gegen die Spitze befahrbar."
)

#signal(
  "Ne 13b",
  text[
    / Beschreibung: Ein orangefarbener waagerechter Streifen über einem orange-weiß schräg gestreiften Mastschild.
  ],
  "Die Rückfallweiche ist gegen die Spitze nicht befahrbar, vor der Weiche halten"
)

#signal(
  "Ne 14",
  text[
    / Beschreibung: Ein gelber Pfeil mit weißem Rand auf einer blauen quadratischen Tafel.
  ],
  "Halt für Züge in ETCS-Betriebsart SR."
)

#signal(
  "So 1",
  text[
    / Beschreibung: Eine viereckige rote Tafel mit liegendem weißen Kreuz.
  ],
  "Fahren auf Sicht beenden."
)

#signal(
  "So 19",
  text[
    / Beschreibung: Drei aufeinander folgende viereckige orangefarbene Tafeln mit einer, zwei oder drei weißen Kreisflächen, deren Anzahl in Fahrtrichtung abnimmt; die Kreisflächen können rückstrahlend sein.
  ],
  "Ein Hauptsignal ist zu erwarten."
)

#signal(
  "So 106",
  text[
    / Beschreibung: Eine weiße Sechseckscheibe mit liegendem schwarzem Kreuz an einem schwarz und weiß schräg gestreiften Pfahl.
  ],
  "Bei fehlendem Vorsignal wird angezeigt, dass ein Hauptsignal zu erwarten ist."
)

== Bü (Signale für Bahnübergänge)

#signal(
  "Bü 0",
  text[
    / Beschreibung: Eine runde gelbe Scheibe in einer gelben Umrahmung über einem schwarz-weiß schräg gestreiften Mastschild. Anstatt der Scheibe und der gelben Umrahmung kann das Signal auch zwei waagerecht angeordnete gelbe Lichter bzw. rückstrahlende Scheiben zeigen.
  ],
  "Halt vor dem Bahnübergang! Weiterfahrt nach Sicherung."
)

#signal(
  "Bü 1",
  text[
    / Beschreibung: Ein blinkendes weißes Licht über einer runden gelben Scheibe in einer gelbenUmrahmung über einem schwarz-weiß schräg gestreiften Mastschild. Anstatt des weißen Blinklichts über der Scheibe in der Umrahmung kann das Signal Bü 1 auch ein weißes Licht über zwei waagerecht angeordneten gelben Lichtern bzw. rückstrahlenden Scheiben zeigen.
  ],
  "Der Bahnübergang darf befahren werden."
)

#signal(
  "Bü 2",
  text[
    / Beschreibung: Eine rechteckige schwarze Tafel mit vier auf den Spitzen übereinander stehenden rückstrahlenden weißen Rauten.
  ],
  "Ein Überwachungssignal ist zu erwarten."
)

#signal(
  "So 15 (DV 301)",
  text[
    / Beschreibung: Eine rechteckige, weiße rückstrahlende Tafel mit drei waagerechten, schwarzen Streifen.
  ],
  "Überwachungssignal beachten."
)

#signal(
  "Bü 3 (DS 301)",
  text[
    / Beschreibung: Eine schwarz-weiß waagerecht gestreifte rückstrahlende Tafel.
  ],
  "Kennzeichnung des Einschaltpunktes von Blinklichtern oder Lichtzeichen mit Fernüberwachung."
)

#signal(
  "So 14 (DV 301)",
  text[
    / Beschreibung: Ein schwarz-weiß waagerecht gestreifter Pfahl.
  ],
  "Kennzeichnung des Einschaltpunktes von Blinklichtern."
)

#signal(
  "Bü 4",
  text[
    / Beschreibung: Eine rechteckige weiße Tafel mit schwarzem P oder eine rechteckige schwarze Tafel mit weißem Rand und weißem P.
  ],
  "Etwa 3 Sekunden lang pfeifen!"
)

#signal(
  "Pf 2",
  text[
    / Beschreibung: Zwei weiße Tafeln mit schwarzem P senkrecht übereinander.
P
  ],
  "Zweimal pfeifen!"
)

#signal(
  "Bü 5",
  text[
    / Beschreibung: Eine rechteckige weiße Tafel mit schwarzem L.
  ],
  "Es ist zu läuten."
)

== Sk (Signalkombinationen)

#signal(
  "Sk 1",
  text[
    / Beschreibung: Ein grünes Licht.
  ],
  "Fahrt (Sk-Hauptsignal), Fahrt erwarten (Sk-Vorsignal), Fahrt, Fahrt erwarten (Sk-Haupt-/Vorsignal)."
)

#signal(
  "Sk 2",
  text[
    / Beschreibung: Ein gelbes Licht.
  ],
  "Halt erwarten (Sk-Vorsignal), Fahrt, Halt erwarten (Sk-Haupt-/Vorsignal)."
)



